require 'byebug'

class Code
  # debugger
  attr_reader :pegs
  PEGS = { r: 'red', g: 'green', b: 'blue', y: 'yellow', o: 'orange', p: 'purple' }

  def initialize(pegs)
    raise 'ArgumentError' if pegs.empty?
    @pegs = pegs
  end

  def [](code)
    pegs[code]
  end

  def ==(other)
    other.is_a?(String) ? false : @pegs == other.pegs
  end

  def self.parse(guess)
    input = guess.downcase.split("")

    input.each { |char| raise 'Wrong colors.' unless PEGS.keys.include?(char.to_sym) }
    Code.new(input)
  end

  def self.random
    rand_code = 4.times.map { PEGS.keys.sample }

    Code.new(rand_code.join)
  end

  def exact_matches(code)
    matches = 0
    code.pegs.each_with_index do |char, idx|
      matches += 1 if char == self[idx]
    end
    matches
  end

  def near_matches(code)
    matches = 0
    already_used = []
    code.pegs.each_with_index do |char, idx|
      idx2 = pegs.index(char)
      if char == self[idx]
        next
      elsif pegs.include?(char) && idx != idx2 && !already_used.include?(idx2)
        matches += 1
        already_used << idx2
      end
    end
    matches
  end
end

class Game
  attr_reader :secret_code

  def initialize(secret_code = Code.random)
    @secret_code = secret_code
  end

  def get_guess
    $stdin = $stdin.string.delete("\n")
    Code.parse($stdin)
  end

  def display_matches(code)
    pegs = code
    exact = @secret_code.exact_matches(pegs)
    near = @secret_code.near_matches(pegs)
    p "You have #{exact} exact matches and #{near}
    near matches"
  end
end
